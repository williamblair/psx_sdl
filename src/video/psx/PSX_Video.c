/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/
#include "SDL_config.h"

/* Dummy SDL video driver implementation; this is just enough to make an
 *  SDL-based application THINK it's got a working video driver, for
 *  applications that call SDL_Init(SDL_INIT_VIDEO) when they don't need it,
 *  and also for use as a collection of stubs when porting SDL to a new
 *  platform for which you haven't yet written a valid video driver.
 *
 * This is also a great way to determine bottlenecks: if you think that SDL
 *  is a performance problem for a given platform, enable this driver, and
 *  then see if your application runs faster without video overhead.
 *
 * Initial work by Ryan C. Gordon (icculus@icculus.org). A good portion
 *  of this was cut-and-pasted from Stephane Peter's work in the AAlib
 *  SDL video driver.  Renamed to "DUMMY" by Sam Lantinga.
 */

#include "SDL_video.h"
#include "SDL_mouse.h"
#include "../SDL_sysvideo.h"
#include "../SDL_pixels_c.h"
#include "../../events/SDL_events_c.h"

//#include "SDL_nullvideo.h"
#include "PSX_Video.h"
//#include "SDL_nullevents_c.h"
//#include "SDL_nullmouse_c.h"
//#include "../dummy/SDL_nullevents_c.h"
//#include "../dummy/SDL_nullmouse_c.h"

//#define DUMMYVID_DRIVER_NAME "dummy"
#define PSXVID_DRIVER_NAME "psx"

static unsigned int prim_list[0x4000];
static int dbuf; // current display buffer
static volatile int time_counter;
static volatile int display_is_old;

static GsSprite screen_sprite; // The screen buffer as a a sprite for the PSX to draw

/* Initialization/Query functions */
static int PSX_VideoInit(_THIS, SDL_PixelFormat *vformat);
static SDL_Rect **PSX_ListModes(_THIS, SDL_PixelFormat *format, Uint32 flags);
static SDL_Surface *PSX_SetVideoMode(_THIS, SDL_Surface *current, int width, int height, int bpp, Uint32 flags);
static int DUMMY_SetColors(_THIS, int firstcolor, int ncolors, SDL_Color *colors);
static void DUMMY_VideoQuit(_THIS);

/* Hardware surface functions */
static int DUMMY_AllocHWSurface(_THIS, SDL_Surface *surface);
static int DUMMY_LockHWSurface(_THIS, SDL_Surface *surface);
static void DUMMY_UnlockHWSurface(_THIS, SDL_Surface *surface);
static void DUMMY_FreeHWSurface(_THIS, SDL_Surface *surface);

/* etc. */
//static void DUMMY_UpdateRects(_THIS, int numrects, SDL_Rect *rects);
static void PSX_UpdateRects(_THIS, int numrects, SDL_Rect *rects);

/* DUMMY driver bootstrap functions */

#if 0
static int DUMMY_Available(void)
{
	const char *envr = SDL_getenv("SDL_VIDEODRIVER");
	if ((envr) && (SDL_strcmp(envr, DUMMYVID_DRIVER_NAME) == 0)) {
		return(1);
	}

	return(0);
}
#endif

static int PSX_Available(void)
{
    const char *envr = SDL_getenv("SDL_VIDEODRIVER");
    printf("Get SDL video driver name: %s\n", envr);
    if ((envr) && (SDL_strcmp(envr, PSXVID_DRIVER_NAME) == 0)) {
        return(1);
    }

    return(0);
}

static void DUMMY_DeleteDevice(SDL_VideoDevice *device)
{
	SDL_free(device->hidden);
	SDL_free(device);
}

static SDL_VideoDevice *DUMMY_CreateDevice(int devindex)
{
	SDL_VideoDevice *device;

    printf("Creating Device!\n");

	/* Initialize all variables that we clean on shutdown */
	device = (SDL_VideoDevice *)SDL_malloc(sizeof(SDL_VideoDevice));
	if ( device ) {
		SDL_memset(device, 0, (sizeof *device));
		device->hidden = (struct SDL_PrivateVideoData *)
				SDL_malloc((sizeof *device->hidden));
	}
	if ( (device == NULL) || (device->hidden == NULL) ) {
		SDL_OutOfMemory();
		if ( device ) {
			SDL_free(device);
		}
		return(0);
	}
	SDL_memset(device->hidden, 0, (sizeof *device->hidden));

	/* Set the function pointers */
	//device->VideoInit = DUMMY_VideoInit;
    device->VideoInit = PSX_VideoInit;
	device->ListModes = PSX_ListModes;
	device->SetVideoMode = PSX_SetVideoMode;
	device->CreateYUVOverlay = NULL;
	device->SetColors = DUMMY_SetColors;
	//device->UpdateRects = DUMMY_UpdateRects;
    device->UpdateRects = PSX_UpdateRects;
	device->VideoQuit = DUMMY_VideoQuit;
	device->AllocHWSurface = DUMMY_AllocHWSurface;
	device->CheckHWBlit = NULL;
	device->FillHWRect = NULL;
	device->SetHWColorKey = NULL;
	device->SetHWAlpha = NULL;
	device->LockHWSurface = DUMMY_LockHWSurface;
	device->UnlockHWSurface = DUMMY_UnlockHWSurface;
	device->FlipHWSurface = NULL;
	device->FreeHWSurface = DUMMY_FreeHWSurface;
	device->SetCaption = NULL;
	device->SetIcon = NULL;
	device->IconifyWindow = NULL;
	device->GrabInput = NULL;
	device->GetWMInfo = NULL;
//	device->InitOSKeymap = DUMMY_InitOSKeymap;
//	device->PumpEvents = DUMMY_PumpEvents;
    device->InitOSKeymap = NULL;
    device->PumpEvents = NULL;

	device->free = DUMMY_DeleteDevice;

    printf("Returning device!\n");

	return device;
}

VideoBootStrap PSX_bootstrap = {
    PSXVID_DRIVER_NAME, "PSX video driver",
    PSX_Available, DUMMY_CreateDevice
};

void prog_vblank_handler() {
    display_is_old = 1;
    time_counter++;
}

/* Update the playstation screen */
static void update_screen(void *screen_buf)
{
    if (display_is_old)
    {
        /* Switch the display and draw areas */
        GsSetDispEnvSimple(0, dbuf ? 0 : 256);
        GsSetDrawEnvSimple(0, dbuf ? 256 : 0, 320, 240);
        
        /* Switch the current display buffer */
        dbuf = !dbuf;
        
        /* Upload the framebuffer image to PSX memory */
        LoadImage(screen_buf, 320, 0, PSX_SCREENBUF_WIDTH, PSX_SCREENBUF_HEIGHT);
        while(GsIsDrawing());
        
        /* Add the screen buffer sprite to the packet list */
        GsSortSprite(&screen_sprite);
        
        /* Draw the packet list */
        GsDrawList();
        while(GsIsDrawing());
        
        /* Set flag to prevent drawing until the image has been
         * set on the T.V. */
        display_is_old = 0;
    }
}

static int PSX_VideoInit(_THIS, SDL_PixelFormat *vformat)
{
    printf("PSX Video Init!\n");
    
    /* Using 16bit depth 
     * The psx uses BGR color format:
     * Bit  16     : Mask (is this alpha?)
     * Bits 15 - 11: Blue
     * Bits 10 - 6 : Green
     * Bits 5  - 1 : Red    */
    vformat->BitsPerPixel = 16;
    vformat->BytesPerPixel = PSX_BPP;
    vformat->Rmask = 0x0000001F;  // 0b0 00000 00000 11111
    vformat->Gmask = 0x000003E0;  // 0b0 00000 11111 00000
    vformat->Bmask = 0x00007C00;  // 0b0 11111 00000 00000
    vformat->Amask = 0x00008000;  // ??? ALPHA???
    
    /* Init PSX stuff */
    PSX_Init();
    GsInit();
    GsSetList(prim_list);
    GsClearMem();
    
    /* Set screen res 
     * Using the ACTUAL playstation's screen resolution (320x240) */
    GsSetVideoMode(PSX_SCREEN_WIDTH, PSX_SCREEN_HEIGHT, PSX_VMODE);
    
    /* Load font for GsPrintFont */
    GsLoadFont(768, 0, 768, 256);
    
    /* Function to be called every time image sent to tv */
    SetVBlankHandler(prog_vblank_handler);
    
    /* Init buffer memory */
    //memset(this->hidden->buffer, 0, PSX_SCREENBUF_WIDTH*PSX_SCREENBUF_HEIGHT*PSX_BPP);
    // bj test!
    int i;
    for (i = 0; i < PSX_SCREENBUF_WIDTH*PSX_SCREENBUF_HEIGHT; i++)
        this->hidden->buffer[i] = 0xFFFF;
    
    screen_sprite.x = 32;
    screen_sprite.y = 0;
    screen_sprite.w = PSX_SCREENBUF_WIDTH;
    screen_sprite.h = PSX_SCREENBUF_HEIGHT;
    screen_sprite.u = 0;
    screen_sprite.v = 0;
    screen_sprite.cx = 0;
    screen_sprite.cy = 0;
    screen_sprite.r = screen_sprite.g = screen_sprite.b = NORMAL_LUMINOSITY;
    screen_sprite.scalex = 2; // 1 times scale
    screen_sprite.scaley = 2;
    screen_sprite.mx = (screen_sprite.w/2) * (screen_sprite.scalex / SCALE_ONE);
    screen_sprite.mx = (screen_sprite.h/2) * (screen_sprite.scaley / SCALE_ONE);
    screen_sprite.rotate = ROTATE_ONE * 0; // rotation
    screen_sprite.tpage = 5; // which texture page (from left to right) the image is stored in - in this case 5 maps to the x,y location (320,0)
    //screen_sprite.attribute = 2; // should be COLORMODE_16BPP
    screen_sprite.attribute = COLORMODE(COLORMODE_16BPP); // should be COLORMODE_16BPP
    
    printf("BJ: SDL_Init: Screen Sprite:\n");
    printf("  x,y: %d,%d\n", screen_sprite.x, screen_sprite.y);
    printf("  w,h: %d,%d\n", screen_sprite.w, screen_sprite.h);
    printf("  u,v: %d,%d\n", screen_sprite.u, screen_sprite.v);
    printf("  cx,cy: %d,%d\n", screen_sprite.cx, screen_sprite.cy);
    printf("  scalex,scaley: %d,%d\n", screen_sprite.scalex, screen_sprite.scaley);
    printf("  tpage: %d\n", screen_sprite.tpage);
    printf("  attribute: %d\n", screen_sprite.attribute);
    
    // BJ TEST - test drawing to the screen
    /*update_screen(this->hidden->buffer);
    update_screen(this->hidden->buffer); // do it twice in case we need to flip buffers before we see anything
    update_screen(this->hidden->buffer); // do it twice in case we need to flip buffers before we see anything
    update_screen(this->hidden->buffer); // do it twice in case we need to flip buffers before we see anything
    update_screen(this->hidden->buffer); // do it twice in case we need to flip buffers before we see anything*/
    //while(1) {
    //    update_screen(this->hidden->buffer);
    //}
    //
    
    /* Set variables */
    this->hidden->w = PSX_SCREENBUF_WIDTH;
    this->hidden->h = PSX_SCREENBUF_HEIGHT;

    printf("End of PSX_VIDEO_INIT!\n");
    
    return 0;
}

/* List of resolutions PSX_ListModes can give */
static SDL_Rect rect_128x120 = {0, 0, PSX_SCREENBUF_WIDTH, PSX_SCREENBUF_HEIGHT};
static SDL_Rect *vesa_modes[] = {
    &rect_128x120,
    NULL
};

static SDL_Rect **PSX_ListModes(_THIS, SDL_PixelFormat *format, Uint32 flags)
{
    /* Currently using 24 bits per pixel */
    int bpp = format->BitsPerPixel;
    if (bpp != 16) {
        return NULL;
    }

    printf("End of psx_listmodes!\n");
    
    /* Just return out 128x120 res */
    return vesa_modes;
}

static SDL_Surface *PSX_SetVideoMode(_THIS, SDL_Surface *current,
                int width, int height, int bpp, Uint32 flags)
{
    printf("SDL_SetVideoMode %d x %d x %d\n", width, height, bpp);
    
    /* Clear video buffer */
    memset(this->hidden->buffer, 0, PSX_SCREENBUF_WIDTH*PSX_SCREENBUF_HEIGHT*PSX_BPP);
    
    /* Test bpp to make sure supported */
    if (bpp != 16) {
        printf("WARNING: Given bpp of %d, using 24 as currently supported!\n");
    }
    
    /* Using 16bit depth 
     * The psx uses BGR color format:
     * Bit  16     : Mask (is this alpha?)
     * Bits 15 - 11: Blue
     * Bits 10 - 6 : Green
     * Bits 5  - 1 : Red    */
    int BitsPerPixel = 16;
    int Rmask = 0x0000001F;   // 0b0 00000 00000 11111
    int Gmask = 0x000003E0;   // 0b0 00000 11111 00000
    int Bmask = 0x00007C00;   // 0b0 11111 00000 00000
    int Amask = 0x00008000;   // ALPHA??? -- Not sure about this ATM...
    
    /* Reallocate settings */
    if (! SDL_ReallocFormat(current, BitsPerPixel, Rmask, Gmask, Bmask, Amask))
    {
        SDL_SetError("Failed to realloc format for bpp %d\n", BitsPerPixel);
        
        return(NULL);
    }
    
    /* Set some info */
    this->hidden->w = current->w = PSX_SCREENBUF_WIDTH;
	this->hidden->h = current->h = PSX_SCREENBUF_HEIGHT;
    current->pitch = current->w * sizeof(unsigned short); 
	current->pixels = this->hidden->buffer;
    
    /* No cursor plz */
    SDL_SetCursor((SDL_Cursor*)NULL);
    
    return current;
}

/* We don't actually allow hardware surfaces other than the main one */
static int DUMMY_AllocHWSurface(_THIS, SDL_Surface *surface)
{
	return(-1);
}
static void DUMMY_FreeHWSurface(_THIS, SDL_Surface *surface)
{
	return;
}

/* We need to wait for vertical retrace on page flipped displays */
static int DUMMY_LockHWSurface(_THIS, SDL_Surface *surface)
{
	return(0);
}

static void DUMMY_UnlockHWSurface(_THIS, SDL_Surface *surface)
{
	return;
}

#if 0
static void DUMMY_UpdateRects(_THIS, int numrects, SDL_Rect *rects)
{
	/* do nothing. */
}
#endif

static void PSX_UpdateRects(_THIS, int numrects, SDL_Rect *rects)
{
    int i;
    
    /* TODO - figure out what needs to be done with rects... */
    /*********************************************************/
    
    /* Update the screen */
    //printf("Updating Screen!\n");
    
    //for (i = 0; i < PSX_SCREENBUF_WIDTH*PSX_SCREENBUF_HEIGHT; i++)
    //    this->hidden->buffer[i] = 0xFFFF;
    
    //int x = 50;
    //int y = 75;
    //this->hidden->buffer[y * PSX_SCREENBUF_WIDTH + x] = 0x001F;
    
    update_screen(this->hidden->buffer);
}

int DUMMY_SetColors(_THIS, int firstcolor, int ncolors, SDL_Color *colors)
{
	/* do nothing of note. */
	return(1);
}

/* Note:  If we are terminated, this could be called in the middle of
   another SDL video routine -- notably UpdateRects.
*/
void DUMMY_VideoQuit(_THIS)
{
	if (this->screen->pixels != NULL)
	{
		SDL_free(this->screen->pixels);
		this->screen->pixels = NULL;
	}
}

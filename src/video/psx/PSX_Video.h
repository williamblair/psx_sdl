/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/
#include "SDL_config.h"

#ifndef _SDL_psxvideo_h
#define _SDL_psxvideo_h

#include "../SDL_sysvideo.h"

#include <psx.h>
#include <stdio.h>

/* Hidden "this" pointer for the video functions */
#define _THIS	SDL_VideoDevice *this


/* Private display data */
#define PSX_SCREENBUF_WIDTH 128      // the framebuffer we're using's size (half of the actual display width/height)
#define PSX_SCREENBUF_HEIGHT 120
#define PSX_SCREEN_WIDTH 320         // the actual size of the psx screen
#define PSX_SCREEN_HEIGHT 240
#define PSX_BPP 2 // bytes per pixel

/* Change if necessary... */
#define PSX_VMODE VMODE_NTSC

struct SDL_PrivateVideoData {
    int w, h; // this will always be 128,120
    //unsigned char buffer[PSX_SCREEN_WIDTH*PSX_SCREEN_HEIGHT*PSX_BPP];
    unsigned short buffer[PSX_SCREEN_WIDTH*PSX_SCREEN_HEIGHT];
};

#endif /* _SDL_psxvideo_h */

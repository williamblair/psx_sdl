#include C:\Compilers\psx-sdk\Makefile.cfg

TOOLCHAIN_PREFIX = /usr/local/psxsdk

CDLIC_FILE = /usr/local/psxsdk/share/licenses/infousa.dat

CC = psx-gcc
AR = mipsel-unknown-elf-ar 
BIN = lib/libSDL.a
CFLAGS = -D __BJ_PSX__ -D stderr=stdio -g -fsigned-char -msoft-float -mno-gpopt -fno-builtin -G0
#CFLAGS = -D __BJ_PSX__ -G0
INCDIRS = -I./include -I./src -I$(TOOLCHAIN_PREFIX)/include
LIBDIRS = -L$(TOOLCHAIN_PREFIX)/lib
LIBS = 
OBJS = src/SDL.o src/SDL_error.o src/SDL_fatal.o \
       src/audio/SDL_audio.o src/audio/SDL_audiocvt.o src/audio/SDL_audiodev.o src/audio/SDL_mixer.o src/audio/SDL_mixer_m68k.o \
       src/audio/SDL_mixer_MMX.o src/audio/SDL_mixer_MMX_VC.o src/audio/SDL_wave.o \
       src/audio/dummy/SDL_dummyaudio.o \
       src/cdrom/SDL_cdrom.o \
       src/cpuinfo/SDL_cpuinfo.o \
       src/events/SDL_active.o src/events/SDL_events.o src/events/SDL_expose.o src/events/SDL_keyboard.o src/events/SDL_mouse.o src/events/SDL_quit.o src/events/SDL_resize.o \
       src/file/SDL_rwops.o \
       src/joystick/SDL_joystick.o \
       src/stdlib/SDL_getenv.o src/stdlib/SDL_iconv.o src/stdlib/SDL_malloc.o src/stdlib/SDL_qsort.o src/stdlib/SDL_stdlib.o src/stdlib/SDL_string.o \
       src/thread/SDL_thread.o \
       src/timer/SDL_timer.o \
       src/video/SDL_blit.o src/video/SDL_blit_0.o src/video/SDL_blit_1.o src/video/SDL_blit_A.o src/video/SDL_blit_N.o src/video/SDL_bmp.o src/video/SDL_cursor.o src/video/SDL_gamma.o src/video/SDL_pixels.o src/video/SDL_RLEaccel.o src/video/SDL_stretch.o src/video/SDL_surface.o src/video/SDL_video.o src/video/SDL_yuv.o src/video/SDL_yuv_mmx.o src/video/SDL_yuv_sw.o \
       src/audio/disk/SDL_diskaudio.o \
       src/video/dummy/SDL_nullevents.o src/video/dummy/SDL_nullmouse.o src/video/dummy/SDL_nullvideo.o \
       src/joystick/dummy/SDL_sysjoystick.o \
       src/cdrom/dummy/SDL_syscdrom.o \
       src/thread/generic/SDL_syscond.o src/thread/generic/SDL_sysmutex.o src/thread/generic/SDL_syssem.o src/thread/generic/SDL_systhread.o \
       src/timer/dummy/SDL_systimer.o \
       src/loadso/dummy/SDL_sysloadso.o \
       src/video/psx/PSX_Video.o
       

all: $(OBJS)
	echo "Done with objs!"
	$(AR) cru $(BIN) $(OBJS)
	
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $< $(INCDIRS)

testprog:
	mkdir -p cd_root
	$(CC) $(CFLAGS) test_sdl.c -o test_sdl.elf $(INCDIRS) -Llib -lSDL
	elf2exe test_sdl.elf testsdl.exe
	cp testsdl.exe cd_root
	systemcnf testsdl.exe > cd_root/system.cnf
	mkisofs -o testsdl.hfs -V TEST -sysid PLAYSTATION cd_root
	mkpsxiso testsdl.hfs testsdl.bin $(CDLIC_FILE)

run:
	pcsxr -cdfile testsdl.bin

clean:
	rm -rf $(OBJS) $(BIN) *.exe *.hfs cd_root/* *.bin *.cue *.elf

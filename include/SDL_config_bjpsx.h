/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#ifndef _SDL_config_bjpsx_h
#define _SDL_config_bjpsx_h

#include "SDL_platform.h"
//#include "video/psx/PSX_Video.h"
/* This is a set of defines to configure the SDL features */

// bj changed
/*typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed short int16_t;
typedef unsigned short uint16_t;
typedef signed int int32_t;
typedef unsigned int uint32_t;
typedef signed long long int64_t;
typedef unsigned long long uint64_t;
typedef unsigned long uintptr_t;*/
//#include "inttypes.h"

#define SDL_HAS_64BIT_TYPE	1

/* Useful headers */
#define HAVE_SYS_TYPES_H	1 // BJ Verified
//#define HAVE_STDIO_H	0     // BJ Verified - doesn't have fprintf or stderr, though
#define STDC_HEADERS	1     // BJ Verified - stdint, stdio, stdlib, string, strings
#define HAVE_STRING_H	1     // BJ Verified
//#define HAVE_CTYPE_H	0     // BJ Verified

/* C library functions */
#define HAVE_MALLOC	1     // BJ Verified
#define HAVE_CALLOC	1     // BJ Verified
#define HAVE_REALLOC	0 // BJ Verified
#define HAVE_FREE	1     // BJ Verified
#define HAVE_ALLOCA	1     // ?? Not sure
//#define HAVE_GETENV	0     // BJ Didn't find
//#define HAVE_PUTENV	0     // BJ Didn't find
#define HAVE_QSORT	1     // BJ Verified
#define HAVE_ABS	1     // BJ Verified
#define HAVE_BCOPY	1     // BJ Verified
#define HAVE_MEMSET	1     // BJ Verified
#define HAVE_MEMCPY	1     // BJ Verified
#define HAVE_MEMMOVE	1 // BJ Verified
#define HAVE_MEMCMP	1     // BJ Verified
#define HAVE_STRLEN	1     // BJ Verified
//#define HAVE_STRDUP	0     // BJ Didn't find
#define HAVE_INDEX	1     // BJ Verified
#define HAVE_RINDEX	1     // BJ Verified
#define HAVE_STRCHR	1     // BJ Verified
#define HAVE_STRRCHR	1 // BJ Verified
#define HAVE_STRSTR	1     // BJ Verified
#define HAVE_STRTOL	1     // BJ Verified
#define HAVE_STRTOD	1     // BJ Verified
//#define HAVE_ATOI	0     // BJ Verified
//#define HAVE_ATOF	0     // ?? Didn't find
#define HAVE_STRCMP	1     // BJ Verified
#define HAVE_STRNCMP	1 // BJ Verified
#define HAVE_STRICMP	1 // BJ Verified
#define HAVE_STRCASECMP	1 // BJ Verified
//#define HAVE_SSCANF	0     // ?? BJ didn't find
#define HAVE_SNPRINTF	1 // BJ Verified
#define HAVE_VSNPRINTF	1 // BJ Verified

// BJ disabled all below for now

/* Enable various audio drivers */
//#define SDL_AUDIO_DRIVER_DC	0
//#define SDL_AUDIO_DRIVER_DISK	0
//#define SDL_AUDIO_DRIVER_DUMMY	0

/* Enable various cdrom drivers */
//#define SDL_CDROM_DC	0

/* Enable various input drivers */
//#define SDL_JOYSTICK_DC	0

/* Enable various shared object loading systems */
//#define SDL_LOADSO_DUMMY	0

/* Enable various threading systems */
//#define SDL_THREAD_DC	0

/* Enable various timer systems */
//#define SDL_TIMER_DC	0

/* Enable various video drivers */
//#define SDL_VIDEO_DRIVER_DC	0
//#define SDL_VIDEO_DRIVER_DUMMY	0

// bj copied from sdl_config minimal
/* Enable the dummy audio driver (src/audio/dummy/\*.c) */
#define SDL_AUDIO_DRIVER_DUMMY	1

/* Enable the stub cdrom driver (src/cdrom/dummy/\*.c) */
#define SDL_CDROM_DISABLED	1

/* Enable the stub joystick driver (src/joystick/dummy/\*.c) */
#define SDL_JOYSTICK_DISABLED	1

/* Enable the stub shared object loader (src/loadso/dummy/\*.c) */
#define SDL_LOADSO_DISABLED	1

/* Enable the stub thread support (src/thread/generic/\*.c) */
#define SDL_THREADS_DISABLED	1

/* Enable the stub timer support (src/timer/dummy/\*.c) */
#define SDL_TIMERS_DISABLED	1

/* Enable the dummy video driver (src/video/dummy/\*.c) */
//#define SDL_VIDEO_DRIVER_DUMMY	1
#define SDL_VIDEO_DRIVER_PSX	1

#endif /* _SDL_config_bjpsx_h */

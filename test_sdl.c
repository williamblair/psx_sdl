#include "SDL.h"

int main(int argc, char *argv[])
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to init SDL: %s\n", SDL_GetError());
        return -1;
    }
    printf("Initialized SDL!\n");
   
    SDL_Surface *screen = SDL_SetVideoMode(128, 120, 16, SDL_HWSURFACE);
    if (!screen) {
        printf("Failed to init screen: %s\n", SDL_GetError());
    }
    printf("Initialized video mode! format bpp: %d\n", screen->format->BitsPerPixel);
    
    // don't show the cursor plz
    SDL_ShowCursor(SDL_DISABLE);
    
    // now wanna try and update the screen...
    
    // test drawing a rect...
    SDL_Rect test_rect;
    test_rect.x = 10;
    test_rect.y = 10;
    test_rect.w = 50;
    test_rect.h = 50;
    
    // just let the screen be displayed...
    while(1) {
        // see what happend i suppose...
        SDL_FillRect(screen, &test_rect, SDL_MapRGB(screen->format, 255, 0, 0));
        
        SDL_Flip(screen);
    }
    
    SDL_Quit();
    return 0;
}
